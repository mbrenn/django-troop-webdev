from django.db import models


class Badge(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.__class__.__name__} {self.name}"


class Rank(Badge):
    order = models.SmallIntegerField(null=True)


class Award(Badge):
    ...


class MeritBadge(Badge):
    eagle_required = models.BooleanField(default=False)
