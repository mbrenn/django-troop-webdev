from django.db import models
from .badge import Badge


class Requirement(models.Model):
    badge = models.ForeignKey(
        Badge,
        on_delete=models.CASCADE,
        related_name="requirements"
    )
    code = models.CharField(max_length=20)
    text = models.CharField(max_length=200, null=True)
    full_text = models.CharField(max_length=1000, null=True)

    def __str__(self):
        return f"{self.__class__.__name__} {self.code}"


class RankRequirement(Requirement):
    ...


class MeritBadgeRequirement(Requirement):
    ...


class AwardRequirement(Requirement):
    ...
