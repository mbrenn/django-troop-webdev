from django.db import models
from datetime import date
from .user import Scout, User
from .requirement import Requirement
from .badge import Badge


class Signoff(models.Model):
    date = models.DateField(default=date.today, null=True)
    scout = models.ForeignKey(Scout, on_delete=models.CASCADE)
    requirement = models.ForeignKey(Requirement, on_delete=models.CASCADE)

    def __str__(self):
        return f"<{self.__class__.__name__} {self.scout.last_name} {self.requirement.code}>"

    class Meta:
        abstract = True


class RankSignoff(Signoff):
    ...


class MeritBadgeSignoff(Signoff):
    ...


class AwardSignoff(Signoff):
    ...
