from django.db import models
from .badge import Badge
from .requirement import Requirement, RankRequirement, MeritBadgeRequirement, AwardRequirement

class RequirementGroup(models.Model):
    name = models.CharField(max_length=20)
    requirements = models.ManyToManyField(
        Requirement,
    )

    def __str__(self):
        return f"{self.__class__.__name__} {self.name}"


class RankRequirementGroup(RequirementGroup):
    ...


class MeritBadgeRequirementGroup(RequirementGroup):
    ...


class AwardRequirementGroup(RequirementGroup):
    ...
