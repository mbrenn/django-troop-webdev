from django.db import models


class Group(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return f"<{self.__class__.__name__} {self.name}>"


class Patrol(Group):
    ...
