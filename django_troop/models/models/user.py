from django.db import models
from .group import Group, Patrol
from .badge import Rank


class User(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    middle_name = models.CharField(max_length=50, null=True)
    nick_name = models.CharField(max_length=50, null=True)
    groups = models.ManyToManyField(Group, related_name="members")

    def __str__(self):
        return f"<{self.__class__.__name__} {self.first_name} {self.last_name}>"

    def whole_name(self):
        if self.nick_name:
            return f'{self.first_name} "{self.nick_name}" {self.last_name}'
        return f"{self.first_name} {self.last_name}"

    def safe_name(self):
        return f"{self.first_name[0]} {self.last_name}"


class Adult(User):
    ...


class Scout(User):
    age = models.PositiveSmallIntegerField(default=0)
    patrol = models.ForeignKey(
        Patrol,
        on_delete=models.CASCADE,
        null=True,
        related_name="scout_members"
    )
    current_rank = models.ForeignKey(
        Rank,
        on_delete=models.CASCADE,
        null=True,
        related_name="current_rank_scouts"
    )
    parents = models.ManyToManyField(Adult)
