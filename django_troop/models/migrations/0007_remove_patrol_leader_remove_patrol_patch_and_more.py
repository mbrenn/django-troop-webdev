# Generated by Django 4.2.4 on 2023-09-10 19:28

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("models", "0006_patrol_leader_patrol_patch_patrol_yell"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="patrol",
            name="leader",
        ),
        migrations.RemoveField(
            model_name="patrol",
            name="patch",
        ),
        migrations.RemoveField(
            model_name="patrol",
            name="yell",
        ),
        migrations.AlterField(
            model_name="awardsignoff",
            name="date",
            field=models.DateField(default=datetime.date.today, null=True),
        ),
        migrations.AlterField(
            model_name="meritbadgesignoff",
            name="date",
            field=models.DateField(default=datetime.date.today, null=True),
        ),
        migrations.AlterField(
            model_name="ranksignoff",
            name="date",
            field=models.DateField(default=datetime.date.today, null=True),
        ),
    ]
