from django.apps import AppConfig


class AdvancementConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "advancement"
