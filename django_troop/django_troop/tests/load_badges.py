from pathlib import Path
import toml
from models.badge import Rank
from models.requirement import RankRequirement

data = toml.load(Path("..") / "data" / "example_troop" / "requirements.toml")

print(data)
