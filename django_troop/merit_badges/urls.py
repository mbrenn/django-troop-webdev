from django.urls import path

from . import views

urlpatterns = [
    path("", views.merit_badges, name="merit_badges"),
]
