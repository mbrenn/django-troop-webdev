from django.shortcuts import render
from models.models import Scout, Rank
from django.http import HttpResponse
from django.template import Context, loader


def index(request):
    return HttpResponse("Hello world you're at the scouts index")

# Create your views here.


def scouts(request):
    # for s in Scout.objects.all():
    #     print(s.whole_name().strip())
    #     print(s.patrol.name.strip())
    #     print(s.current_rank.name.strip())
    #     print("\n".join([f"{signoff.requirement.code} {signoff.date}" for signoff in s.ranksignoff_set.all()]))

    return render(request, "index.html")
