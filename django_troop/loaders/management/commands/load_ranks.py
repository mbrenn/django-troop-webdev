import csv

from pathlib import Path
import toml

from django.core.management.base import BaseCommand

from models.models.badge import Rank
from models.models.requirement import RankRequirement
base = "data/example_troop"


def load_ranks(path):
    data = toml.load(path)

    for rank, rank_data in data.items():
        print(rank)
        r, created = Rank.objects.get_or_create(name=rank, order=rank_data["rank_order"])
        if created:
            r.save()
        for code, more_data in rank_data["requirements"].items():
            print(code)
            req, created = RankRequirement.objects.get_or_create(
                badge=r, code=code, text=more_data["text"], full_text=more_data["full_text"]
            )
            if created:
                req.save()


class Command(BaseCommand):
    help="Loads ranks and requirements"
    
    def add_arguments(self, parser): 
        parser.add_argument("base_dir", nargs='?', default=base, type=str)

    def handle(self, *args, **kwargs):
        base_dir = Path(kwargs['base_dir'])

        load_ranks(base_dir / "requirements.toml")


def main():
    Command().handle(base_dir=base)

