import csv
from pathlib import Path
from django.core.management.base import BaseCommand
from models.models.group import Patrol

base = "data/example_troop"


def load_patrols(path):

    with open(path) as f:
        reader = csv.DictReader(f)
        for line in reader:
            print(line["Name"])
            patrol, created = Patrol.objects.get_or_create(name=line["Patrol"])
            if created:
                patrol.save()


class Command(BaseCommand):
    help="Loads patrols"
    
    def add_arguments(self, parser): 
        parser.add_argument("base_dir", nargs='?', default=base, type=str)

    def handle(self, *args, **kwargs):
        base_dir = Path(kwargs['base_dir'])
        load_patrols(path=base_dir / 'example_troop_patrols.csv')

def main():
    Command().handle(base_dir=base)
