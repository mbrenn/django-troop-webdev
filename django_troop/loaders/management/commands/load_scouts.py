import csv

from pathlib import Path
import toml

from django.core.management.base import BaseCommand

from models.models.user import Scout
from models.models.badge import Rank
from models.models.group import Patrol
from models.models.signoff import RankSignoff

base = "data/example_troop"


def load_scouts(path):
    with open(path) as f:
        reader = csv.DictReader(f)
        for line in reader:
            print(line["Name"])
            patrol, created = Patrol.objects.get_or_create(name=line["Patrol"])
            if created:
                patrol.save()

            scout, created = Scout.objects.get_or_create(last_name=line["Name"])
            scout.patrol = Patrol.objects.get(name=line['Patrol'])
            scout.current_rank = Rank.objects.get(name=line["Current Rank"])
            try:
                scout.age = int(line["Age"])
            except ValueError as e:
                scout.age = 1
                print(f"invalid scout age: {e}")
            scout.save()


class Command(BaseCommand):
    help="Loads scouts, patrols"
    
    def add_arguments(self, parser): 
        parser.add_argument("base_dir", nargs='?', default=base, type=str)

    def handle(self, *args, **kwargs):
        base_dir = Path(kwargs['base_dir'])

        load_scouts(base_dir / "example_troop_scouts.csv")


def main():
    Command().handle(base_dir=base)

