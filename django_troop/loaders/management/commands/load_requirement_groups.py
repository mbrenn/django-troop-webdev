import csv

from pathlib import Path
import toml

from django.core.management.base import BaseCommand

from models.models.badge import Rank
from models.models.requirement import RankRequirement
from models.models.requirement_group import (RankRequirementGroup,
                                             MeritBadgeRequirementGroup,
                                             )

base = "data/example_troop"


def load_requirement_groups(path):
    data = toml.load(path)
    for name, group_data in data.items():
        print(f"Importing {name}")
        if group_data['type'] == "rank":
            group, created = RankRequirementGroup.objects.get_or_create(name=name)
            if created:
                group.save()
            fill_rank_requirement_group(group, name, group_data)
        elif group_data['type'] == "merit_badge":
            group_created = MeritBadgeRequirementGroup.objects.get_or_create(name=name)
            if created:
                group.save()
            fill_merit_badge_requirement_group(group, name, group_data)


def fill_rank_requirement_group(group, name, data):
    for rank_name, requirement_list in data['requirements'].items():
        r = Rank.objects.get(name=rank_name)
        for requirement_code in requirement_list:
            req = RankRequirement.objects.get(badge=r, code=requirement_code)
            group.requirements.add(req)
    group.save()


def fill_merit_badge_requirement_group(group, name, group_data):
    ...


class Command(BaseCommand):
    help="Loads requirement groups"
    
    def add_arguments(self, parser): 
        parser.add_argument("base_dir", nargs='?', default=base, type=str)

    def handle(self, *args, **kwargs):
        base_dir = Path(kwargs['base_dir'])

        load_requirement_groups(base_dir / "requirement_groups.toml")


def main():
    Command().handle(base_dir=base)
