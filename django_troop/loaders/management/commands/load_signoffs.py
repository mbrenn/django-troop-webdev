import csv

from pathlib import Path
import toml
from datetime import datetime

from django.core.management.base import BaseCommand

from models.models.badge import Rank
from models.models.requirement import RankRequirement
from models.models.user import Scout
from models.models.signoff import RankSignoff

base = "data/example_troop"


def load_rank_signoffs(path):
    with open(path) as f:
        reader = csv.DictReader(f)
        for line in reader:
            print(line)
            if line["Date Earned"]:
                rank = Rank.objects.get(name=line["Rank"])
                scout = Scout.objects.get(last_name=line["Scout"])
                requirement = RankRequirement.objects.get(badge=rank,
                                                          code=line["Code"])
                signoff = RankSignoff(scout=scout,
                                      requirement=requirement,
                                      date=datetime.strptime(line["Date Earned"],
                                                             "%Y-%m-%d").date())
                signoff.save()
                print(signoff)


class Command(BaseCommand):
    help = "Loads signoffs"

    def add_arguments(self, parser):
        parser.add_argument("base_dir", nargs='?', default=base, type=str)

    def handle(self, *args, **kwargs):
        base_dir = Path(kwargs['base_dir'])

        load_rank_signoffs(base_dir / "example_troop_rank_signoffs.csv")


def main():
    Command().handle(base_dir=base)
