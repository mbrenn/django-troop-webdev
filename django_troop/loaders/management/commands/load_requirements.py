import csv

from pathlib import Path
import toml

from django.core.management.base import BaseCommand

from models.models.badge import Rank
from models.models.requirement import RankRequirement
from models.models.user import Scout
from models.models.group import Patrol
from models.models.signoff import RankSignoff

base = "data/example_troop"


def load_ranks_and_requirements(path):
    data = toml.load(path)

    for rank, rank_data in data.items():
        print(rank)
        r, created = Rank.objects.get_or_create(name=rank, order=rank_data["rank_order"])
        if created:
            r.save()
        for code, more_data in rank_data["requirements"].items():
            print(code)
            req, created = RankRequirement.objects.get_or_create(
                badge=r, code=code, text=more_data["text"], full_text=more_data["full_text"]
            )
            if created:
                req.save()

def load_patrols_and_scouts(path):

    with open(path) as f:
        reader = csv.DictReader(f)
        for line in reader:
            print(line["Name"])
            patrol, created = Patrol.objects.get_or_create(name=line["Patrol"])
            if created:
                patrol.save()

            scout, created = Scout.objects.get_or_create(last_name=line["Name"])
            scout.patrol = Patrol.objects.get(name=line['Patrol'])
            scout.current_rank = Rank.objects.get(name=line["Current Rank"])
            try:
                scout.age = int(line["Age"])
            except ValueError as e:
                scout.age = 1
                print(f"invalid scout age: {e}")
            scout.save()


def load_rank_signoffs(path):
    with open(path) as f:
        reader = csv.DictReader(f)
        for line in reader:
            print(line)
            if line["Date Earned"]:
                rank = Rank.objects.get(name=line["Rank"])
                scout = Scout.objects.get(last_name=line["Scout"])
                requirement = RankRequirement.objects.get(badge=rank, code=line["Code"])
                signoff = RankSignoff(scout=scout, requirement=requirement)
                signoff.save()
                print(signoff)


class Command(BaseCommand):
    help="Loads ranks, requirements, scouts, patrols, and signoffs"
    
    def add_arguments(self, parser): 
        parser.add_argument("base_dir", nargs='?', default=base, type=str)

    def handle(self, *args, **kwargs):
        base_dir = Path(kwargs['base_dir'])

        load_ranks_and_requirements(base_dir / "requirements.toml")
        load_patrols_and_scouts(base_dir / "example_troop_scouts.csv")
        load_rank_signoffs(base_dir / "example_troop_rank_signoffs.csv")


