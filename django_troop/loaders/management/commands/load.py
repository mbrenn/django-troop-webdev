import csv
from pathlib import Path
from django.core.management.base import BaseCommand

from .load_ranks import main as load_ranks
from .load_scouts import main as load_scouts
from .load_patrols import main as load_patrols
from .load_signoffs import main as load_signoffs
from .load_requirement_groups import main as load_requirement_groups

base = "data/example_troop"

class Command(BaseCommand):
    help="Loads ranks, requirements, scouts, patrols, and signoffs"
    
    def add_arguments(self, parser): 
        parser.add_argument("base_dir", nargs='?', default=base, type=str)

    def handle(self, *args, **kwargs):
        base_dir = Path(kwargs['base_dir'])

        load_ranks()
        load_requirement_groups()
        load_patrols()
        load_scouts()
        load_signoffs()
