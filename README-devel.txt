For developers of django-troop

Overall Vision - create an open-source, free, self-hostable equivalent to commercial BSA management software like Troopwebhost, Troopmaster and Scoutbook. 

Goals - 

Advancement management - be able to record completion of BSA rank, merit badge and other awards advancement

Troop Roster - be able to manage personal information for members of the troop both scouts and adults

Calendar - be able to manage troop events on a calendar, including having people sign up for events to be listed as wanting to attend and then later as having attended. 

Reports - be able to report on advancement or attendance for scouts or adults. 

Testing - the code should have automated unit and integration tests to ensure it can be easily checked

Technologies - 

Docker - The project should be deployable using docker or docker-compose. Once running, the project should be accessible via https access on a local port. 

Python - The principle language for the project is Python.  Compatibility with Python 3.10 is a must. 

Django - A "batteries included" web framework for Python.  Compatibility with Django 4.2

HTMX - Augments normal HTML to allow more elements in the document to be interactive and to allow things other than the entire document to be loaded. 

CSS framework - I haven't decided on Bootstrap 5.0 vs W3CSS. W3CSS is likely sufficient and is significant lighter. 

SQLite (for now) - simplicity and being able to host this simply. 

Configuration variables will be stored in a TOML file. 

Long-term or exported storage of information for persistance beyond a database will be in CSV. 

Code will be formatted using Black. 

Type hinting is not required (for now). 

Autodoc with Sphinx. 

We will use Django's provided testing framework. 

Semantic versioning - Major.Minor.Bugfix

Overall license is MIT. 

